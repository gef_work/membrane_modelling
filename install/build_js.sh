#!/bin/bash -e

install_dir=`pwd`
build_dir=${install_dir}/build

if [ -f "${HOME}/.emscripten" ]; then
  echo ""
  echo "  It appears that emscripten is already installed!"
  echo "  As a precaution, please backup your ~/.emscripten"
  echo "    manually if appropriate before running this"
  echo "    script!"
  echo ""

  exit 1
fi

if [ ! -d "${build_dir}" ]; then
  mkdir ${build_dir}
  cd ${build_dir}
else
  echo ""
  echo "  Build directory ${build_dir} already exists!"
  echo "  As a precaution, please delete ${build_dir}" 
  echo "    manually before running this script, or"
  echo "    modify this script!"
  echo ""

  exit 1
fi

git_dir=${build_dir}/git

if [ ! -d "${git_dir}" ]; then
  mkdir ${git_dir}
fi

pushd ${git_dir}

if [ ! -d "emscripten/.git" ]; then
  echo "  Cloning emscripten"
  git clone https://github.com/kripken/emscripten.git
fi

if [ ! -d "emscripten-fastcomp/.git" ]; then
  echo "  Cloning emscripten-fastcomp"
  git clone https://github.com/kripken/emscripten-fastcomp.git
fi

pushd emscripten-fastcomp

if [ ! -d "tools/clang/.git" ]; then
  echo "  Cloning emscripten-fastcomp-clang"
  git clone https://github.com/kripken/emscripten-fastcomp-clang tools/clang  

  mkdir build
  pushd build

  ../configure --enable-optimized --disable-assertions --enable-targets=host,js
  make -j8

  popd
fi

popd   # emscripten-fastcomp

popd   # git_dir

pushd ${build_dir}

node_version=v0.10.24

if [ ! -f "node-${node_version}/node" ]; then
  echo "  Downloading an old node release"
  wget https://nodejs.org/dist/${node_version}/node-${node_version}.tar.gz
  tar -zxf node-${node_version}.tar.gz
  cd node-${node_version}
  ./configure
  make -j8

  rm -f node-${node_version}.tar.gz
fi

popd   # build_dir

export PATH=${PATH}:${git_dir}/emscripten
export EMSCRIPTEN=${git_dir}/emscripten
export LLVM=${git_dir}/emscripten-fastcomp/build/Release/bin
export NODE=${build_dir}/node-${node_version}/node

gsl_dir=${build_dir}/gsl
gsl_lib_dir=${gsl_dir}/lib
gsl_include_dir=${gsl_dir}/include

pushd ${build_dir}

if [ ! -f "${gsl_lib_dir}/libgsl.a" ]; then

  if [ ! -d "${gsl_dir}" ]; then
    mkdir ${gsl_dir}
    mkdir ${gsl_include_dir}
    mkdir ${gsl_lib_dir}
  fi

  pushd ${gsl_dir}

  wget http://ftp.gnu.org/gnu/gsl/gsl-1.16.tar.gz

  tar -zxf gsl-1.16.tar.gz

  pushd gsl-1.16

  emconfigure ./configure

  emmake make all -j8

  cp gsl/*.h ${gsl_include_dir}/

  cp .libs/libgsl.a ${gsl_lib_dir}/
  cp cblas/.libs/libgslcblas.a ${gsl_lib_dir}/
  cp err/.libs/libgslerr.a ${gsl_lib_dir}/
  cp histogram/.libs/libgslhistogram.a ${gsl_lib_dir}/
  cp rng/.libs/libgslrng.a ${gsl_lib_dir}/

  popd   # gsl-1.16

  rm -f gsl-1.16.tar.gz
  rm -rf gsl-1.16

  popd  # gsl
fi

popd  # build_dir

bc_dir=${build_dir}/bc
src1_dir=${install_dir}/src1

pushd ${build_dir}

if [ ! -f "${bc_dir}/generate_config.bc" ]; then

  mkdir -p ${bc_dir} 

  pushd ${bc_dir}

  cp -v ${src1_dir}/* .
  ln -s ${gsl_include_dir} gsl

  emcc -v -Wall -o generate_config.bc generate_config.cpp -L${gsl_lib_dir} -lgsl -lgslblas -I./

  popd   # bc_dir
fi
 
popd   # build_dir

js_dir=${build_dir}/js
src2_dir=${install_dir}/src2

pushd ${build_dir}

if [ ! -f "${js_dir}/final.js" ]; then

  mkdir -p ${js_dir}
  pushd ${js_dir}
  cp -v ${src2_dir}/* .
  ln -s ${gsl_include_dir} gsl

  emcc -v ${bc_dir}/generate_config.bc -o final.js --pre-js pre.js --post-js post.js \
       --embed-file obstacles_hist.dat -L${gsl_lib_dir} -lgsl -lgslblas -lgslrng -lgslerr -lgslhistgram \
       -s ASSERTIONS=1 -s EXPORTED_FUNCTIONS="['_fish','_main','_chip']" -s NO_EXIT_RUNTIME=1 -s EXCEPTION_DEBUG=1

  popd   # js_dir
fi

popd   # build_dir

run_dir=${install_dir}/run

pushd ${install_dir}

cp -v ${js_dir}/final.js ${run_dir}/js

popd
