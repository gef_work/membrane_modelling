# Demonstration interface for the membrane modelling group

As the researchers were using C++ and [Gnuplot](http://www.gnuplot.info/) in their regular
activities this project was to investigate whether the use of [Emscripten](http://kripken.github.io/emscripten-site/)
could generate a javascript-only web interface to their membrane modelling (rather than
needing to make system call-outs to disk invocations or other services).  
Although it was a useful example with which to demonstrate and inform the decision-making
process a more conventional route using a 3rd-party (almost!) contractor to create a 
python-based application was chosen as the researchers decided that that was the route 
which they wanted to take.  



Example screenshot :

![screenshot](https://bitbucket.org/gef_work/membrane_modelling/raw/master/screenshot.png "Application screenshot")


This project makes use of the following :

 * Gnuplot javascript derived from [gnuplot-JS](https://github.com/chhu/gnuplot-JS) and [www](http://gnuplot.respawned.com/)
   javascript. (I tried myself to build my own Gnuplot.js but couldn't - so used this version).
 * [emscripten](https://github.com/kripken/emscripten), including [emscripten-fastcomp](https://github.com/kripken/emscripten-fastcomp)
   and [emscripten-fastcomp-clang](https://github.com/kripken/emscripten-fastcomp-clang).
 * [GNU Scientific Library](http://www.gnu.org/software/gsl/) - Used by the researcher's code.
 * [Node.js](https://nodejs.org) - as required by emscripten.
 * Python's `SimpleHTTPServer` as the application server.

## Requirements

### hardware

At least 2Gb of disk space, >= 4 cores, and probably >= 4Gb RAM.

### build

The install script (see below) will download all the necessary software (emscripten, emscripten-fastcomp, emscripten-fastcomp-clang
and gsl (GNU Scientific Library)) into a (git-ignored) subdirectory and try to build everything to create the application javascript.  

 * git
 * gcc-c++ (>4.7 `g++ -v`)
 * wget

### run

 * python

## Install

See [INSTALL.md](INSTALL.md "Install instructions") to create the `final.js` javascript, which if successfully built by the install
script will automatically be copied to the `install/run/js/` directory.  
(A reserve `final.js` exists in [install/run/js](https://bitbucket.org/gef_work/membrane_modelling/raw/master/install/run/js/) as 
`backup_final.js` if preferred - simply rename it to `final.js`.)

 * `cd install/run/`
 * `python -m SimpleHTTPServer 7000`
 * Visit http://localhost:7000/index.html
 
The first thing that should appear in the `Science` tab pages should be `_main is loaded`.  

The `Generate Config` button creates new `Data` for display. The `Data` and `Plot script` boxes do have default content... it's just
that for some reason (which wasn't investigated), if they initially appear blank when running the application you need to focus the
mouse in the boxes and press the 'Page Down' button on your keyboard - or something similar.