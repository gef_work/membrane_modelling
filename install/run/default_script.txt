# plot_config.txt
#
# Usage:
# gnuplot plot_config.txt
#
# output plot file
#filename="config.png"
# input data file with configurations
#datafile="obst_conf.dat"

# these parameters in principle could be taken from the datafile (first 3 lines)
num_out=1
Npart=200 
epsilon=0.01

set style circle radius graph epsilon
set style line 1 linewidth 2
#set style fill solid 0.0 border;
set style fill solid;
set xrange [-0.5:0.5];
set yrange [-0.5:0.5];
#set term postscript enhanced color eps;
#set terminal png
#set terminal png size 1000,1000
set terminal svg size 400,300 enhanced fname 'arial'  fsize 10 butt solid
#set output filename
set output 'out.svg'
set size square
set key off
unset xtics
unset ytics
# plot datafile every ::Nmovi::(Nmovi+Nobst) using 1:2 with circles lt rgb "red" lw .1
#plot datafile using 1:2 with circles lt rgb "red" lw .1
plot "data.txt" using 1:2 with circles lt rgb "red" lw .1
#quit

