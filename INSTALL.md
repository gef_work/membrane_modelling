# Installation instructions

The build script is designed to not run if it detects that there's a `~/.emscripten` file or that
a build attempt has already taken place - just to play safe!  

*NOTE* : If there's no `~/.emscripten` file then on the first emscripten invocation during the build
process it will create it and force the build to exit - in which case simply comment out the two
`exit 1` instructions in the build script and re-run.

## Building the javascript

 1. `cd install`
 1. `./build_js.sh`  
    Note: This step could take 30+ minutes! It takes a while to download the emscripten, and the
    script isn't clever enough to detect the number of processors and is currently expecting 8
    cores, i.e. `-j8`.
