#define EM_ASM_REEXPAND(x) EM_ASM(x)

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <sstream>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <time.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_histogram2d.h>
#include "emscripten.h"
#include "vectorNdim.h"

//To compile: g++ -Wall -o generate_config generate_config.cpp -I./ -lgsl -lgslcblas

//Functions prototypes//
void build_density(gsl_histogram2d_pdf **p);
void create_conf(vectorNdim<double, 2> *config, int Npart, double epsilon, const gsl_histogram2d_pdf *p);
extern "C" void fish();
extern "C" void chip();

// Variable definitions//
int Npart = 200;//!< Number of particles
double epsilon = 0.01; // particles' radius
int num_out = 1; //number of obstacles configuration to output

static gsl_rng *rng;

int main()
{
    EM_ASM(
      FS.syncfs(true, function(err) {
                        if (err)
                        {
                          alert('ERROR1! ' + err);
                        }
                        else
                        {
                          Module.ccall('fish');
                        }
                      });
    );

    EM_ASM(
      FS.syncfs(true, function(err) {
                        if (err)
                        {
                          alert('ERROR2! ' + err);
                        }
                        else
                        {
                          Module.ccall('chip');
                        }
                      });
    );

    return 0;
}

void create_conf(vectorNdim<double, 2> *config, int Npart, double epsilon, const gsl_histogram2d_pdf *p)
{
    long int maxattempts = 10000 * Npart, attempts = 0;
    bool any_overlap, overlap;
    double u1, u2, x, y;
    double epsilon2 = 4 * epsilon * epsilon;
    vectorNdim<double, 2> rij;

    for (int i = 0; i < Npart; i++)
    {
        do
        {
            u1 = gsl_rng_uniform(rng); u2 = gsl_rng_uniform(rng);
            gsl_histogram2d_pdf_sample(p, u1, u2, &x, &y);

            config[i] = vectorNdim<double, 2>(x, y);
            any_overlap = false;
            for (int j = 0; j < i; ++j)
            {
                rij = config[i] - config[j];
                if (rij.norm2() < epsilon2)
                {
                    overlap = true;
                }
                else
                {
                    overlap = false;
                }

                any_overlap = any_overlap || overlap;
            }
            attempts++;
            if (attempts >= maxattempts)
            {
                std::cerr << "random insertions did not work - max attempts reached!!!\n" << std::flush;
                exit(0);
            }
        }
        while (any_overlap);
    }
}

void build_density(gsl_histogram2d_pdf **p)
{
    double a, b; //limits

    size_t nbins; //number of bins (nx=ny=n)
    FILE *input0;

    //load data of histogram
    if ( !(input0 = fopen("obstacles_hist.dat", "r") ))
    {
        std::cerr << "error in opening obstacles_hist.dat file.\n";
    }

    //read first line of the file to store limits and number of bins

    fscanf(input0, "%lf", &a); fscanf(input0, "%lf", &b);
    fscanf(input0, "%lf", &a); fscanf(input0, "%lf", &b);
    fscanf(input0, "%lu", &nbins);

    gsl_histogram2d *h = gsl_histogram2d_alloc(nbins, nbins); // histogram structure
    gsl_histogram2d_set_ranges_uniform(h, a, b, a, b);

    if (gsl_histogram2d_fscanf(input0, h) != 0)
    {
        std::cerr << "error reading hist for s(x).\n";
        exit(1);
    }

    *p = gsl_histogram2d_pdf_alloc(nbins, nbins); // prob density structure

    if (gsl_histogram2d_pdf_init (*p , h) != 0)
    {
        std::cerr << "error initialising p from h \n";
        exit(1);
    }

    gsl_histogram2d_free (h);
}

void chip() 
{
    /*
    std::string line;
    std::ifstream myfile("example.txt");
    if (myfile.is_open())
    {
      while (getline(myfile, line))
      {
        std::cerr << line << "\n";
      }
      myfile.close();
    }
    else 
    {
      printf("chip - example.txt could not be opened\n");
    }
    */

    gsl_rng_env_setup();
    rng = gsl_rng_alloc(gsl_rng_default);
    FILE *file2;
    if (( file2 = fopen("gsl_state.dat", "r") ))
    {
        gsl_rng_fread(file2, rng);
        fclose(file2);
    }
}

void fish()
{
    //******* SETUP OF RNG (random number generator) *********/
    gsl_rng_env_setup();
    rng = gsl_rng_alloc(gsl_rng_default);
    FILE *file2;
    if (( file2 = fopen("gsl_state.dat", "r") ))
    {
        gsl_rng_fread(file2, rng);
        fclose(file2);
    }

    //printf("Random num generation: seed = %lu, first value = %lu\n", gsl_rng_default_seed, gsl_rng_get (rng));
    std::cout << "Random num generation: seed = " << gsl_rng_default_seed << ", first value = " << gsl_rng_get(rng) << std::endl;

    // ********open file for outputing the configurations **** //
    /*
    std::string filename = "obst_conf.dat";
    std::ofstream conf_out(filename.c_str()); assert(conf_out.is_open());
    conf_out << "num_out " << num_out << "\n";
    conf_out << "Npart " << Npart << "\n";
    conf_out << "epsilon " << epsilon << "\n";
    */
    std::cerr << "num_out " << num_out << std::endl;
    std::cerr << "Npart " << Npart << std::endl;
    std::cerr << "epsilon " << epsilon << std::endl;

    //******** construct density of obstacles *************//
    gsl_histogram2d_pdf *p; // prob density structure
    build_density(&p); // construct density from a histogram

    vectorNdim<double, 2>  obstpos;
    gsl_histogram2d_pdf_sample(p, gsl_rng_uniform(rng), gsl_rng_uniform(rng), &obstpos[0], &obstpos[1]);

    //printf("Sample from p: %f %f\n", obstpos[0], obstpos[1]);
    std::cout << "Sample from p: " << obstpos[0] << " " << obstpos[1] << std::endl;

    vectorNdim<double, 2> *config;
    config = new vectorNdim<double, 2>[Npart]; // to store obstacle configuration

    for (int i = 0; i < num_out; i++)
    {
        create_conf(config, Npart, epsilon, p);
        for (int j = 0; j < Npart; j++)
        {
            //conf_out << config[j] << std::endl;
            std::cerr << config[j] << std::endl;
        }
        //conf_out << std::endl;
        std::cerr << std::endl;
    }

    gsl_histogram2d_pdf_free (p);

    delete[] config;

    FILE *file;
    if (( file = fopen("gsl_state.dat", "w") ))
    {
        gsl_rng_fwrite(file, rng);
        fclose(file);
    }

    gsl_rng_free(rng);

    /*
    std::ofstream myfile;
    myfile.open("example.txt", std::fstream::app);
    myfile << "Stuff.\n";
    myfile.close();
    */

    EM_ASM(
      FS.syncfs(function (err) {
                  assert(!err);
                });
    );
}
