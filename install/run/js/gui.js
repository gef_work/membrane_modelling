/**
 * 
 */

gnuplot.init('gnuplot.js');
gnuplot.text = "";
gnuplot.interval = setInterval(function() {
	updateListing(false);
}, 3000);

gnuplot.files = [];

var lastDataValue = '';
var lastPlotValue = '';
var dct = 0;
var pct = 0;
var rt = 0;

function updateListing(updateImages) {
	if (!gnuplot.worker)
		return;
	gnuplot.getListing(function(e) {
		var l = e.content;
		gnuplot.lastListing = l;
		$("#listing").empty();
		var html_l = "";
		$("#vfsHeading").html("VFS contents (" + l.length + ")");
		for (var name in l)
			html_l += "<a href='#' onclick='gnuplot.getFile(\"" + l[name] + "\", function(evt) {downloadAsFile(\"" + l[name] + "\", new Uint8Array(evt.content).buffer);})' value='" + l[name] + "' class='list-group-item'><span onclick=\"deleteFile(event, '" + l[name] + "')\" class=\"glyphicon glyphicon-minus-sign\">&nbsp;</span>" + l[name] + "</a>";
		$("#listing").html(html_l);
		// Search for an image
		if (!updateImages)
			return;
		for (var nameidx in l) {
			var name = l[nameidx];
			if (name.search(".png") > 0)
		        gnuplot.getFile(name, function(e) {
		            if (!e.content) {
		                gnuplot.onError("Output file "+name + " not found!");
		                return;
		            }
		            toGnuplotImage(e.content, "image/png");
		        });
			if (name.search(".jpg") > 0 || name.search(".jpeg") > 0)				
		        gnuplot.getFile(name, function(e) {
		            if (!e.content) {
		                gnuplot.onError("Output file "+name + " not found!");
		                return;
		            }
		            toGnuplotImage(e.content, "image/jpeg");
		        });
			if (name.search(".gif") > 0)
		        gnuplot.getFile(name, function(e) {
		            if (!e.content) {
		                gnuplot.onError("Output file "+name + " not found!");
		                return;
		            }
		            toGnuplotImage(e.content, "image/gif");
		        });
			if (name.search(".svg") > 0)
		        gnuplot.getFile(name, function(e) {
		            if (!e.content) {
		                gnuplot.onError("Output file "+name + " not found!");
		                return;
		            }
		            toGnuplotImage(e.content, "image/svg+xml");
		            var gpi = $("#gnuplotImage")[0];
		            var canv = $("#gnuplotCanvas")[0];
		            //gpi.style.width="100%";
		            
		            setTimeout(function() {
			            canv.width = gpi.width + 140;
			            canv.height = gpi.height+140;
		            
		            setTimeout(function() {
			            var ctx = canv.getContext("2d");
			            ctx.drawImage(gpi, 0, 0);
			            $("#gnuplotRaster")[0].src = canv.toDataURL("image/png");
			            gpi.style.width="100%";
		            }, 200)}, 200);
		        });
		}

	});
}

dataCM.on("change", function(cm, change) {
	var newDataValue = dataCM.getValue();
	if (newDataValue == lastDataValue)
		return;
	lastDataValue = newDataValue;
	clearTimeout(dct);
	dct = setTimeout(function(){
		gnuplot.putFile("data.txt", lastDataValue);
		clearTimeout(rt);
		rt = setTimeout(run, 500);
	}, 500);
});

plotCM.on("change", function(cm, change) {
	var newPlotValue = plotCM.getValue();
	if (newPlotValue == lastPlotValue)
		return;
	lastPlotValue = newPlotValue;
	
	clearTimeout(pct);
	pct = setTimeout(function(){
		gnuplot.putFile("script.txt", lastPlotValue);
		clearTimeout(rt);
		rt = setTimeout(run, 500);
	}, 500);
});

function toGnuplotImage(data, mimestring) {
    var img = document.getElementById('gnuplotImage');
    try {
        var ab = new Uint8Array(data);
        var blob = new Blob([ab], {"type": mimestring});
        window.URL = window.URL || window.webkitURL;
        var urlBlob = window.URL.createObjectURL(blob);
        img.src = urlBlob;
        return urlBlob;
    } catch (err) { // in case blob / URL missing, fallback to data-uri
        if (!window.blobalert) {
            alert('Warning - your browser does not support Blob-URLs, using data-uri with a lot more memory and time required. Err: ' + err);
            window.blobalert = true;
        }
        var rstr = '';
        for (var i = 0; i < e.content.length; i++)
            rstr += String.fromCharCode(e.content[i]);
        img.src = 'data:'+mimestring+';base64,' + btoa(rstr);
    }
};

function run() {
	gnuplot.text = "";
	$("#outDiv").addClass("hidden");
    // "upload" files to worker thread
    for (var f in gnuplot.files)
        gnuplot.putFile(f, gnuplot.files[f]);
	
	gnuplot.run(["script.txt"], function(e) {
		updateListing(true);
	});
};


$('#loadModal').on('show.bs.modal', function (e) {
	var saves = JSON.parse(localStorage["gnuplot.saves"] || "{}");
	var inner = "";
	for (var key in saves) {
		var desc = saves[key];
		inner += '<a href="#" class="list-group-item"  data-dismiss="modal" onclick="restore(localStorage[\'gnuplot.' + key + '\'])"><h4 class="list-group-item-heading">' + key + '</h4><p class="list-group-item-text">'+desc+'</p></a>';
	}
	if (inner)
		$("#loadGroup")[0].innerHTML = inner; 
});

$('#demoModal').on('show.bs.modal', function (e) {
	Echo.init({});
	$.ajax({url: "site/data/1.dat"}).done(function(d) {gnuplot.files["1.dat"] = d;});
	$.ajax({url: "site/data/2.dat"}).done(function(d) {gnuplot.files["2.dat"] = d;});
	$.ajax({url: "site/data/3.dat"}).done(function(d) {gnuplot.files["3.dat"] = d;});
	$.ajax({url: "site/data/arrowstyle.dat"}).done(function(d) {gnuplot.files["arrowstyle.dat"] = d;});
	$.ajax({url: "site/data/asciimat.dat"}).done(function(d) {gnuplot.files["asciimat.dat"] = d;});
	$.ajax({url: "site/data/battery.dat"}).done(function(d) {gnuplot.files["battery.dat"] = d;});
	$.ajax({url: "site/data/big_peak.dat"}).done(function(d) {gnuplot.files["big_peak.dat"] = d;});
	$.ajax({url: "site/data/blutux.rgb"}).done(function(d) {gnuplot.files["blutux.rgb"] = d;});
	$.ajax({url: "site/data/candlesticks.dat"}).done(function(d) {gnuplot.files["candlesticks.dat"] = d;});
	$.ajax({url: "site/data/cities.dat"}).done(function(d) {gnuplot.files["cities.dat"] = d;});
	$.ajax({url: "site/data/clip14in.dat"}).done(function(d) {gnuplot.files["clip14in.dat"] = d;});
	$.ajax({url: "site/data/ctg-y2.dat"}).done(function(d) {gnuplot.files["ctg-y2.dat"] = d;});
	$.ajax({url: "site/data/delaunay-edges.dat"}).done(function(d) {gnuplot.files["delaunay-edges.dat"] = d;});
	$.ajax({url: "site/data/demo.edf"}).done(function(d) {gnuplot.files["demo.edf"] = d;});
	$.ajax({url: "site/data/density.fnc"}).done(function(d) {gnuplot.files["density.fnc"] = d;});
	$.ajax({url: "site/data/ellipses.dat"}).done(function(d) {gnuplot.files["ellipses.dat"] = d;});
	$.ajax({url: "site/data/empty-circles.dat"}).done(function(d) {gnuplot.files["empty-circles.dat"] = d;});
	$.ajax({url: "site/data/energy_circles.dat"}).done(function(d) {gnuplot.files["energy_circles.dat"] = d;});
	$.ajax({url: "site/data/files.txt"}).done(function(d) {gnuplot.files["files.txt"] = d;});
	$.ajax({url: "site/data/finance.dat"}).done(function(d) {gnuplot.files["finance.dat"] = d;});
	$.ajax({url: "site/data/fit3.dat"}).done(function(d) {gnuplot.files["fit3.dat"] = d;});
	$.ajax({url: "site/data/glass.dat"}).done(function(d) {gnuplot.files["glass.dat"] = d;});
	$.ajax({url: "site/data/GM1_bonds.r3d"}).done(function(d) {gnuplot.files["GM1_bonds.r3d"] = d;});
	$.ajax({url: "site/data/GM1_sugar.pdb"}).done(function(d) {gnuplot.files["GM1_sugar.pdb"] = d;});
	$.ajax({url: "site/data/heatmaps.dem"}).done(function(d) {gnuplot.files["heatmaps.dem"] = d;});
	$.ajax({url: "site/data/hemisphr.dat"}).done(function(d) {gnuplot.files["hemisphr.dat"] = d;});
	$.ajax({url: "site/data/hexa.fnc"}).done(function(d) {gnuplot.files["hexa.fnc"] = d;});
	$.ajax({url: "site/data/histerror.dat"}).done(function(d) {gnuplot.files["histerror.dat"] = d;});
	$.ajax({url: "site/data/histopt.dat"}).done(function(d) {gnuplot.files["histopt.dat"] = d;});
	$.ajax({url: "site/data/immigration.dat"}).done(function(d) {gnuplot.files["immigration.dat"] = d;});
	$.ajax({url: "site/data/klein.dat"}).done(function(d) {gnuplot.files["klein.dat"] = d;});
	$.ajax({url: "site/data/labelplot.pdb"}).done(function(d) {gnuplot.files["labelplot.pdb"] = d;});
	$.ajax({url: "site/data/lcdemo.dat"}).done(function(d) {gnuplot.files["lcdemo.dat"] = d;});
	$.ajax({url: "site/data/lena-keypoints.bin"}).done(function(d) {gnuplot.files["lena-keypoints.bin"] = d;});
	$.ajax({url: "site/data/lena.rgb"}).done(function(d) {gnuplot.files["lena.rgb"] = d;});
	$.ajax({url: "site/data/line.fnc"}).done(function(d) {gnuplot.files["line.fnc"] = d;});
	$.ajax({url: "site/data/moli3.dat"}).done(function(d) {gnuplot.files["moli3.dat"] = d;});
	$.ajax({url: "site/data/nearmap.csv"}).done(function(d) {gnuplot.files["nearmap.csv"] = d;});
	$.ajax({url: "site/data/optimize.dat"}).done(function(d) {gnuplot.files["optimize.dat"] = d;});
	$.ajax({url: "site/data/orbital_elements.dat"}).done(function(d) {gnuplot.files["orbital_elements.dat"] = d;});
	$.ajax({url: "site/data/random-points"}).done(function(d) {gnuplot.files["random-points"] = d;});
	$.ajax({url: "site/data/reflect.fnc"}).done(function(d) {gnuplot.files["reflect.fnc"] = d;});
	$.ajax({url: "site/data/rgb_variable.dat"}).done(function(d) {gnuplot.files["rgb_variable.dat"] = d;});
	$.ajax({url: "site/data/scatter2.bin"}).done(function(d) {gnuplot.files["scatter2.bin"] = d;});
	$.ajax({url: "site/data/scatter2.dat"}).done(function(d) {gnuplot.files["scatter2.dat"] = d;});
	$.ajax({url: "site/data/silver.dat"}).done(function(d) {gnuplot.files["silver.dat"] = d;});
	$.ajax({url: "site/data/sine.bin"}).done(function(d) {gnuplot.files["sine.bin"] = d;});
	$.ajax({url: "site/data/sound2.par"}).done(function(d) {gnuplot.files["sound2.par"] = d;});
	$.ajax({url: "site/data/sound.par"}).done(function(d) {gnuplot.files["sound.par"] = d;});
	$.ajax({url: "site/data/soundvel.dat"}).done(function(d) {gnuplot.files["soundvel.dat"] = d;});
	$.ajax({url: "site/data/srl.dat"}).done(function(d) {gnuplot.files["srl.dat"] = d;});
	$.ajax({url: "site/data/start.par"}).done(function(d) {gnuplot.files["start.par"] = d;});
	$.ajax({url: "site/data/stat.inc"}).done(function(d) {gnuplot.files["stat.inc"] = d;});
	$.ajax({url: "site/data/steps.dat"}).done(function(d) {gnuplot.files["steps.dat"] = d;});
	$.ajax({url: "site/data/table.dat"}).done(function(d) {gnuplot.files["table.dat"] = d;});
	$.ajax({url: "site/data/timedat.dat"}).done(function(d) {gnuplot.files["timedat.dat"] = d;});
	$.ajax({url: "site/data/triangle.dat"}).done(function(d) {gnuplot.files["triangle.dat"] = d;});
	$.ajax({url: "site/data/using.bin"}).done(function(d) {gnuplot.files["using.bin"] = d;});
	$.ajax({url: "site/data/using.dat"}).done(function(d) {gnuplot.files["using.dat"] = d;});
	$.ajax({url: "site/data/whale.dat"}).done(function(d) {gnuplot.files["whale.dat"] = d;});
	$.ajax({url: "site/data/world.cor"}).done(function(d) {gnuplot.files["world.cor"] = d;});
	$.ajax({url: "site/data/world.dat"}).done(function(d) {gnuplot.files["world.dat"] = d;});
});

function loadDemo(name) {
	console.log("LD " + name);

	$('#demoModal').modal('hide');
	Echo.stop();
	$.ajax({
	  url: "site/" + name + ".gnu"
	}).done(function( text ) {
	  Echo.stop();
	  plotCM.setValue("set terminal svg size 600,400 enhanced fname 'arial'  fsize 10 butt solid\n" +  
			  		  "set output 'out.svg'\n" + text);
	});
	
}

//arrayBuffer to array for JSON.stringify
function abToArray(ab) {
	var bb = new Uint8Array(ab);
	var arr = new Array(bb.length);
	for (var i=0; i < bb.length; i++)
		arr[i] = bb[i];
	return arr;
}

//array to ab 
function arrayToAb(arr) {
	return new Uint8Array(arr).buffer;
}

function persist() {
	var __files = {};
	for (var k in gnuplot.files) {
		var contents = gnuplot.files[k];
		if (typeof(contents) == "string")
			__files[k] = contents;
		else if (contents instanceof ArrayBuffer) 
			__files[k] = abToArray(contents);
		else
			console.log("Unknown file type " + k);
	}
	__files['script.txt'] = plotCM.getValue();
	__files['data.txt'] = dataCM.getValue();
	
	return __files;
};

function restore(json) {
	var __files = JSON.parse(json);
	gnuplot.files = [];
	for (var k in __files) {
		var content = __files[k];
		if (k == "script.txt") {
			plotCM.setValue(content);
			continue;
		}
		if (k == "data.txt") {
			dataCM.setValue(content);
			continue;
		}
		if (content instanceof Array)
			content = arrayToAb(content);
		gnuplot.files[k] = content;
	}
	run();
};

function handleFileSelect(evt) {
    var _files = evt.target.files; // FileList object

    // files is a FileList of File objects. List some properties.
    var output = [];
    for (var i = 0, f; f = _files[i]; i++) {
        output.push('<li><strong>', escape(f.name), '</strong> (', f.type || 'n/a', ') - ',
                f.size, ' bytes, last modified: ',
                f.lastModifiedDate ? f.lastModifiedDate.toLocaleDateString() : 'n/a',
                '</li>');
        (function() {
            var reader = new FileReader();
            var fname = f.name;
            reader.onloadend = function(e) {
                if (e.target.result) {
                    //gnuplot.onOutput(fname + ": Read success - storing in browser. " + e.target.result.byteLength);
                    //fcont = e.target.result;
                    gnuplot.files[fname] = e.target.result;
                    //localStorage["gnuplot.files"] = JSON.stringify(files);
                    run();
                }

            };
            reader.readAsArrayBuffer(f);
        })();
    }
}

// Service functions
function downloadAsFile(filename, data) {
    window.URL = window.URL || window.webkitURL;
    var a = document.createElement('a');
    var blob = new Blob([data], {'type': 'application\/octet-stream'});
    a.href = window.URL.createObjectURL(blob);
    a.download = filename;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
};

function deleteFile(evt, name) {
	if (evt)
		evt.stopPropagation();
	delete gnuplot.files[name];
	gnuplot.removeFile(name);
	updateListing(false);
}

function deleteAll() {
	for (var i in gnuplot.files)
		deleteFile(null, i);
	
}

function guiSaveBrowserClick() {
	var name = $("#inputSaveName").val() || ("unnamed - " + Date.now());
	
	var desc = $("#inputSaveDesc").val() + " - " + Date();
	var saves = JSON.parse(localStorage['gnuplot.saves'] || "{}");
	if (saves[name])
		if (!confirm(name + " exists in storage. Overwrite?"))
			return;
	saves[name] = desc;
	localStorage["gnuplot." + name] = JSON.stringify(persist());
	localStorage["gnuplot.saves"] = JSON.stringify(saves);
};

function guiLoadFile(files) {
    var file = files[0];
    var reader = new FileReader();
    reader.onload = function(json) { 
    	restore(json.target.result);
    	$('#loadModal').modal('hide');
    };
    reader.readAsText(file);
}

function guiSaveFileClick() {
	var name = $("#inputSaveName").val() || ("unnamed - " + Date.now());
	downloadAsFile(name + ".json", JSON.stringify(persist()));
};

gnuplot.onOutput = function(text) {
	gnuplot.onError(text);
};

gnuplot.onError = function(text) {
	$("#outDiv").removeClass("hidden");
	gnuplot.text = gnuplot.text + "<br>" + text;
	$("#gnuplotOut").html(gnuplot.text);
};

setTimeout(function(){$('input[type=file]').bootstrapFileInput();}, 10);
document.getElementById('files').addEventListener('change', handleFileSelect, false);
$.ajax({
	  url: "site/ps/prologue.ps"
	}).done(function( text ) {
		gnuplot.files["prologue.ps"] = text;
	});

$.ajax({url: "default_script.txt"}).done(function(d) {plotCM.setValue( d);});
$.ajax({url: "default_data.txt"}).done(function(d) {dataCM.setValue(d);});

setTimeout(function(){
setInterval(function() {
	localStorage['gnuplot.last'] = JSON.stringify(persist());
}, 20000);
}, 20000);




/*
files = {};
if (localStorage["gnuplot.files"])
    files = JSON.parse(localStorage["gnuplot.files"]);
for (var key in files)
    gnuplot.onOutput("Found locally stored file: " + key + " with " + files[key].length + " bytes.");
var runScript = function() {
    var editor = document.getElementById('gnuplot');   // textarea
    var start = Date.now();
    // "upload" files to worker thread
    for (var f in files)
        gnuplot.putFile(f, files[f]);

    gnuplot.run(editor.value, function(e) {
        gnuplot.onOutput('Execution took ' + (Date.now() - start) / 1000 + 's.');
        gnuplot.getFile('out.svg', function(e) {
            if (!e.content) {
                gnuplot.onError("Output file out.svg not found!");
                return;
            }
            var img = document.getElementById('gnuimg');
            try {
                var ab = new Uint8Array(e.content);
                var blob = new Blob([ab], {"type": "image\/svg+xml"});
                window.URL = window.URL || window.webkitURL;
                img.src = window.URL.createObjectURL(blob);
            } catch (err) { // in case blob / URL missing, fallback to data-uri
                if (!window.blobalert) {
                    alert('Warning - your browser does not support Blob-URLs, using data-uri with a lot more memory and time required. Err: ' + err);
                    window.blobalert = true;
                }
                var rstr = '';
                for (var i = 0; i < e.content.length; i++)
                    rstr += String.fromCharCode(e.content[i]);
                img.src = 'data:image\/svg+xml;base64,' + btoa(rstr);
            }
        });
    });
};
// set the script from local storage
if (localStorage["gnuplot.script"])
    document.getElementById('gnuplot').value = localStorage["gnuplot.script"];
scriptChange();
function handleFileSelect(evt) {
    var _files = evt.target.files; // FileList object

    // files is a FileList of File objects. List some properties.
    var output = [];
    for (var i = 0, f; f = _files[i]; i++) {
        output.push('<li><strong>', escape(f.name), '</strong> (', f.type || 'n/a', ') - ',
                f.size, ' bytes, last modified: ',
                f.lastModifiedDate ? f.lastModifiedDate.toLocaleDateString() : 'n/a',
                '</li>');
        (function() {
            var reader = new FileReader();
            var fname = f.name;
            reader.onloadend = function(e) {
                if (e.target.result) {
                    gnuplot.onOutput(fname + ": Read success - storing in browser. " + e.target.result.length);
                    files[fname] = e.target.result;
                    localStorage["gnuplot.files"] = JSON.stringify(files);
                }

            };
            reader.readAsText(f);
        })();
    }
    document.getElementById('list').innerHTML = '<ul>' + output.join('') + '</ul>';
}
document.getElementById('files').addEventListener('change', handleFileSelect, false);

*/
